package ma.bingo.shared.domain;

public class JuegoIniciado extends DomainEvent {

    public static final String EVENTO_ORIGEN = "bolillero";
    public static final String EVENTO_NOMBRE = "juego.iniciado";

    public JuegoIniciado() {
        super(EVENTO_ORIGEN, EVENTO_NOMBRE);
    }
}
