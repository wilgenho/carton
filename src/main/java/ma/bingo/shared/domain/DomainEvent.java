package ma.bingo.shared.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public abstract class DomainEvent {

    private String creado;
    private String id;
    private String origen;
    private String evento;

    public DomainEvent(String origen, String evento) {
        this.origen = origen;
        this.evento = evento;
        creado = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        id = UUID.randomUUID().toString();
    }

    public String getCreado() {
        return creado;
    }

    public String getId() {
        return id;
    }

    public String getOrigen() {
        return origen;
    }

    public String getEvento() {
        return evento;
    }

}
