package ma.bingo.shared.infra;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Configuration
public class RabbitMQConfig {

    public static final String EXCHANGE_BOLILLERO = "bolillero";
    public static final String QUEUE_CARTON = "carton-yo";
    public static final String QUEUE_JUEGO = "juego-yo";
    public static final String TOPIC_JUEGO = "juego.iniciado";
    public static final String TOPIC_BOLILLA = "bolilla.quitada";

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory cf) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(cf);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Declarables bindings() {
        Collection<Declarable> list = new ArrayList<>();

        list.add(new Queue(QUEUE_JUEGO, true, false, false));
        list.add(new Binding(QUEUE_JUEGO, QUEUE, EXCHANGE_BOLILLERO, TOPIC_JUEGO, null));

        list.add(new Queue(QUEUE_CARTON, true, false, false));
        list.add(new Binding(QUEUE_CARTON, QUEUE, EXCHANGE_BOLILLERO, TOPIC_BOLILLA, null));

        return new Declarables(list);
    }

    @Bean
    public TopicExchange bolilleroExchange() {
        return new TopicExchange(EXCHANGE_BOLILLERO);
    }


}
