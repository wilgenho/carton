package ma.bingo.carton.application;

import lombok.extern.log4j.Log4j2;
import ma.bingo.carton.domain.Carton;
import ma.bingo.carton.domain.CartonRepository;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CrearCarton {

    private CartonRepository cartonRepository;

    public CrearCarton(CartonRepository cartonRepository) {
        this.cartonRepository = cartonRepository;
    }

    public void crear() {
        Carton carton = new Carton();
        cartonRepository.crear(carton);
        log.info("Elegimos carton nuevo: {}" , carton.getNumeros());
    }
}
