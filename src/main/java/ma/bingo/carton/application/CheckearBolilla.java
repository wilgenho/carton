package ma.bingo.carton.application;

import lombok.extern.log4j.Log4j2;
import ma.bingo.carton.domain.Carton;
import ma.bingo.carton.domain.CartonRepository;
import ma.bingo.shared.domain.EventBus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CheckearBolilla {

    private CartonRepository cartonRepository;
    private EventBus eventBus;

    public CheckearBolilla(CartonRepository cartonRepository, EventBus eventBus) {
        this.cartonRepository = cartonRepository;
        this.eventBus = eventBus;
    }

    public void checkear(int bolilla) {
        Carton carton = cartonRepository.consultar();
        if (carton == null) {
            log.warn("No tengo ningun carton");
            return;
        }
        if (carton.checkear(bolilla)) {
            log.info("Tengo la bolilla {}!", bolilla);
        }
        if (carton.bingo()) {
            log.info("Bingo!");
        }
        eventBus.publish(carton.pullDomainEvents());
    }
}
