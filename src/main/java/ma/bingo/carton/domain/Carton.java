package ma.bingo.carton.domain;

import ma.bingo.shared.domain.AggregateRoot;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Carton extends AggregateRoot {

    private static final int MIN = 1;
    private static final int MAX = 20;
    private static final int CANTIDAD = 5;
    private static final String JUGADOR = "yo";
    private List<Integer> numeros;
    private Integer asiertos = 0;

    public Carton() {
        List<Integer> todos = IntStream.rangeClosed(MIN, MAX).boxed().collect(Collectors.toList());
        Collections.shuffle(todos);
        numeros = todos.subList(0, CANTIDAD);
        asiertos = 0;
    }

    public boolean checkear(int bolilla) {
        if (numeros.contains(bolilla)) {
            asiertos += 1;
            return true;
        }
        return false;
    }

    public boolean bingo() {
        if (asiertos == CANTIDAD) {
            this.record(new Bingo(JUGADOR));

            return true;
        }
        return false;
    }

    public List<Integer> getNumeros() {
        return numeros;
    }
}
