package ma.bingo.carton.domain;


public interface CartonRepository {

    Carton consultar();
    void crear(Carton carton);
}
