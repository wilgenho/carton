package ma.bingo.carton.suscriber;

import lombok.extern.log4j.Log4j2;
import ma.bingo.carton.application.CrearCarton;
import ma.bingo.shared.domain.JuegoIniciado;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class JuegoIniciadoSuscriber {

    private CrearCarton crearCarton;

    public JuegoIniciadoSuscriber(CrearCarton crearCarton) {
        this.crearCarton = crearCarton;
    }

    @RabbitListener(queues = "juego-yo")
    public void iniciar(JuegoIniciado juego) {
        log.info("Se inicia el juego...");
        crearCarton.crear();
    }

}
