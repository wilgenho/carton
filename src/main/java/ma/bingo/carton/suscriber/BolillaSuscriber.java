package ma.bingo.carton.suscriber;

import lombok.extern.log4j.Log4j2;
import ma.bingo.carton.application.CheckearBolilla;
import ma.bingo.shared.domain.BolillaSacada;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class BolillaSuscriber {

    private CheckearBolilla checkearBolilla;

    public BolillaSuscriber(CheckearBolilla checkearBolilla) {
        this.checkearBolilla = checkearBolilla;
    }

    @RabbitListener(queues = "carton-yo")
    public void siguienteBolilla(BolillaSacada bolillaSacada) {
        log.info("salio la bolilla {}" , bolillaSacada.getBolilla());
        checkearBolilla.checkear(bolillaSacada.getBolilla());
    }
}
