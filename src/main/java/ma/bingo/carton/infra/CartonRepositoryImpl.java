package ma.bingo.carton.infra;

import ma.bingo.carton.domain.Carton;
import ma.bingo.carton.domain.CartonRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CartonRepositoryImpl implements CartonRepository {

    private Carton carton;

    @Override
    public Carton consultar() {
        return carton;
    }

    @Override
    public void crear(Carton carton) {
        this.carton = carton;
    }
}
